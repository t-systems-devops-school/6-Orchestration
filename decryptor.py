"""
    install python 
    install cryptography package using pip
    replace the key and encrypted data in the code
    use the script to decrypt the data
    > python3 decryptor.py
"""

import base64
from cryptography.fernet import Fernet

def main():
    # Replace this value with your actual encryption key
    encryption_key = 'xxxx'
    # Replace this value with the encrypted data in Base64 format received via email
    encrypted_data_base64 = 'xxxx'
    try:
        decrypted_data = decrypt_data(encrypted_data_base64, encryption_key)
        print("Decrypted data:", decrypted_data)
    except Exception as e:
        print(f"An error occurred during decryption: {e}")

def decrypt_data(encrypted_data_base64, key):
    encrypted_data = base64.b64decode(encrypted_data_base64)
    cipher_suite = Fernet(key)
    decrypted_data = cipher_suite.decrypt(encrypted_data)
    return decrypted_data.decode('utf-8')

if __name__ == "__main__":
    main()